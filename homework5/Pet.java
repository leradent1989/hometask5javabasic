package homework5;

import java.util.Arrays;

class Pet  {

    private Species species;
    private  String nickName;
    private  int age;
    private   byte trickLevel;
    private  String [] habits;

    public Pet(){}

    public Pet(Species species,String nickname){
        this.species = species;
        this.nickName = nickname;

    }
    public Pet(Species species,String nickName,int age,byte trickLevel){
        this.species = species;
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;

    }
    public  Species  getSpecies(){
        return  species;

    }

    public void setSpecies(Species petSpecies){
        species = petSpecies;
    }

    public  String  getNickName(){
        return  nickName;

    }
    public void setNickName(String petNickname){
        nickName = petNickname;
    }
    public  int  getAge(){
        return  age;

    }

    public void setAge(int petAge){
        age = petAge;
    }
    public  byte  getTrickLevel(){
        return  trickLevel;

    }

    public void setTrickLevel(byte petTrickLevel){
        trickLevel = petTrickLevel;
    }

    public  String []  getHabits(){
        return  habits;

    }

    public void setHabits(String [] petHabits){
        habits = petHabits;
    }

    public static void eat( ){

        System.out.println("Я кушаю");

    }

    public static void foul (){
        System.out.println("Нужно хорошо замести следы...");
    }

    public  void respond(Pet pet){
        System.out.println("Привет хозяин Я"  + " " + pet.getNickName() +" " + "соскучился");
    }
    @Override
    public   String  toString(){

        String message =  species + "{nickname=" +nickName +", age=" + age +", trickLevel=" + trickLevel +" , habits=" + Arrays.toString(habits) + "}" ;
        System.out.println(message);
        return message;
    }
    @Override
    public int hashCode(){
        int result = this.getNickName() == null?0:this.getNickName().hashCode();
        result = result +age;
        result = result + (int) trickLevel;
        return result;
    }
    @Override
    public boolean equals(Object obj){

        if(obj == null){
            return  false;
        }
        if(!(obj.getClass() == Pet.class)){
            return false;
        }
        Pet pet = (Pet) obj;
        int petAge = pet.getAge();
        String petNickName = pet.getNickName();
        Species petSpecies = pet.getSpecies();
        if(petAge == this.age  &&
                (petNickName == this.nickName || petNickName.equals(this.nickName))  &&
                (petSpecies == this.species || petSpecies.equals(this.species))) {
            return true;
        }else  return false;

    }

    @Override

    protected   void finalize() throws Throwable {
        try {
            this.toString();

        } finally {

            super.finalize();       }   }

    public static void main  (String [] Args) throws Throwable {

        Pet Vasia = new Pet();
        Vasia.setSpecies(Species.FISH);
        System.out.println(Vasia.getSpecies());
        Vasia.nickName ="Vasiliy";
        Vasia.age = 3;
        Vasia.trickLevel = 10;
        Vasia.habits  =  new  String [] {"eat","sleep","drink"};

        Vasia.finalize();

    }

}

enum  Species {
    CAT,
    DOG,
    BIRD,
    FISH,
    MOUSE
}