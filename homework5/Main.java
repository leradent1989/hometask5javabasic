package homework5;

public class Main {
    public static void main(String[] args) {

        Runtime runtime = Runtime.getRuntime();
        System.out.println("total memory before " + runtime.totalMemory());
        System.out.println("free memory before " + runtime.freeMemory());
        for(int i = 0; i< 1000000;i++){
            Human human = new Human();
            human = null;

        }
        System.out.println("total memory after " + runtime.totalMemory());
        System.out.println("free memory after " + runtime.freeMemory() );
        System.gc();
        System.out.println("total memory after gc " + runtime.totalMemory());
        System.out.println("free memory after gc " + runtime.freeMemory());


    }
}
