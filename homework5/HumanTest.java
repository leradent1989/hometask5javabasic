package homework5;


import org.junit.jupiter.api.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTest {
    private Human module;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    @Test
    public void  humanToString(){

        module = new Human("human","surname",1995);
        String actual = module.toString();
        String expected = "Human{name = human surname = surname year = 1995 iq = 0 schedule = null}";
        assertEquals(expected,actual);
    }
    @Test

    public void testHumanGreetPet(){
        PrintStream old=System.out;
        Human human=new Human();
        Human mother = new Human();
        Human father = new Human();
        Family Smith = new Family(mother,father);
        Pet pet = new Pet (Species.CAT,"Mura",5,(byte) 60);
        Smith.setPet(pet);
        System.setOut(new PrintStream(output));
        human.greetPet(Smith);
        assertEquals(output.toString().replaceAll("\n",""),"Привет Mura","Successfully brings text");

        System.setOut(old);
    }
    @Test

    public void testHumanDescribePet(){
        PrintStream old=System.out;
        Human human=new Human();
        Human mother = new Human();
        Human father = new Human();
        Family Smith = new Family(mother,father);
        Pet pet = new Pet (Species.CAT,"Mura",5,(byte) 60);
        Smith.setPet(pet);
        System.setOut(new PrintStream(output));
        human.describePet(Smith);
        assertEquals(output.toString().replaceAll("\n",""),"У меня есть CAT, ему 5 лет, он очень хитрый","Successfully brings text");

        System.setOut(old);
    }
}